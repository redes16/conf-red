#Interfaces de Red
#Windows
Para realizar la configuración de las interfaces, seguiremos estos pasos:
Utilizamos la combinación de teclas “Windows + R” buscamos Powershell y lo ejecutamos cómo administrador.
Ejecutamos el siguiente comando:
netsh
Navegamos hasta el apartado de interface:
netsh> interface
Mostramos las interfaces que tenemos disponibles:
netsh interface>show interface
Si queremos que nuestra dirección ip sea estática, ejecutamos el siguiente comando:
netsh interface>ipv4
set address eth0 static X.X.X.X (dirección ip) X.X.X.X (máscara de subred) X.X.X.X (puerta de enlace)
Procedemos a establecer un servidor de DNS dinámico:
set dns servers eth0 source=dhcp
También podemos deshabilitar/habilitar nuestra interfaz, por si queremos reiniciarla, con el siguiente comando:
set interface eth0 enable
Si queremos volver atrás de algún apartado, ejecutamos los dos puntos:
netsh interface>..
netsh>


#Linux
ip dinámica: Debian / Ubuntu / Linux Mint
$ sudo cat /etc/network/interfaces
iface eth0 inet dhcp

IP Estática: Debia|n / Ubuntu / Linux Mint
$ sudo cat /etc/network/interfaces
iface eth0 inet static
address 192.168.1.23
network 192.168.1.0
netmask 255.255.255.0
broadcast 192.168.1.255
gateway 192.168.1.1
hwaddress 81:ab:1c:59:aa:7b

[802-3-ethernet]
mac-address=88:AE:1D:69:5A:5A
[connection]
id=wired-11
uuid=212274c7-08bc-4586-9h49-c1u218p9239f
type=802-3-ethernet
timestamp=1424866869

[ipv6]
method=auto

[ipv4]
method=manual
dns=8.8.8.8;8.8.4.4;
address1=192.168.1.50/24,192.168.1.1
